<?php

/**

 * @author	Tiia Lehikoinen, Kati Kallioniemi
 *
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['nav_search'] = 'Ricerca';
$lang['nav_addgarment'] = 'Aggiungi indumento';
$lang['nav_createoutfit'] = 'Crea Outfit';
$lang['nav_yourgarments'] = 'I tuoi indumenti';
$lang['nav_login'] = 'Accesso';
$lang['nav_profile'] = 'Profilo';
$lang['nav_accountsettings'] = 'Impostazioni dell account';
$lang['nav_signout'] = 'Disconnessione';
$lang['login_form'] = 'Modulo di accesso';
$lang['login_username'] = 'Nome utente';
$lang['login_password'] = 'Parola d\'ordine';
$lang['login_link'] = 'Per l\'accesso clicca qui';
$lang['register_form'] = 'Formulario di registrazione';
$lang['register_email'] = 'E-mail';
$lang['register_passwordconf'] = 'Conferma password';
$lang['register_signup'] = 'Registrazione';
$lang['register_link'] = 'Per iscriversi, clicca qui';
$lang['upload_selectimage'] = 'Seleziona immagine';
$lang['upload_garmentname'] = 'Nome dell \'indumento';
$lang['upload_description'] = 'Descrizione';
$lang['upload_category'] = 'Categoria';
$lang['upload_legwear'] = 'Abbigliamento';
$lang['upload_jacket'] = 'Giacca';
$lang['upload_shirt'] = 'Camicia';
$lang['upload_knit'] = 'Maglia';
$lang['upload_dress'] = 'Vestito';
$lang['upload_shoes'] = 'Scarpe';
$lang['upload_accessories'] = 'Accessori';
$lang['upload_addgarment'] = 'Aggiungi indumento';
$lang['account_updatedata'] = 'Aggiorna la tua Userdata';
$lang['account_newusername'] = 'Nuovo nome utente';
$lang['account_newpassword'] = 'Nuova password';
$lang['account_newemail'] = 'Nuova email';
$lang['account_updatesubmit'] = 'Aggiornare';
$lang['account_delete'] = 'Elimina il tuo account utente';
$lang['account_deletesubmit'] = 'Elimina';
$lang['canvas_savebutton'] = 'Salva tela';
$lang['canvas_imagebutton'] = 'Esporta in PNG';
$lang['canvas_clearbutton'] = 'Canvas chiaro';
$lang['canvas_tooltip_remove_white'] = 'Rimuovi sfondo bianco';
$lang['canvas_tooltip_remove_bg_color'] = 'Seleziona il colore di sfondo da rimuovere';

$lang['upload_addoutfit'] = 'Salva l\'attrezzatura';
