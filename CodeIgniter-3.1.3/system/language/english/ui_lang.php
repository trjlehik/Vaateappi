<?php

/**

 * @author	Tiia Lehikoinen
 *
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['nav_search'] = 'Search';
$lang['nav_addgarment'] = 'Add Garment';
$lang['nav_createoutfit'] = 'Create Outfit';
$lang['nav_yourgarments'] = 'Your Garments';
$lang['nav_login'] = 'Login';
$lang['nav_profile'] = 'Profile';
$lang['nav_accountsettings'] = 'Account Settings';
$lang['nav_signout'] = 'Sign Out';
$lang['login_form'] = 'Login Form';
$lang['login_username'] = 'Username';
$lang['login_password'] = 'Password';
$lang['login_link'] = 'For Login Click Here';
$lang['register_form'] = 'Registration Form';
$lang['register_email'] = 'Email';
$lang['register_passwordconf'] = 'Password Confirmation';
$lang['register_signup'] = 'Sign Up';
$lang['register_link'] = 'To Sign Up Click Here';
$lang['upload_selectimage'] = 'Select Image';
$lang['upload_garmentname'] = 'Garment Name';
$lang['upload_description'] = 'Description';
$lang['upload_category'] = 'Category';
$lang['upload_legwear'] = 'Legwear';
$lang['upload_jacket'] = 'Jacket';
$lang['upload_shirt'] = 'Shirt';
$lang['upload_knit'] = 'Knit';
$lang['upload_dress'] = 'Dress';
$lang['upload_shoes'] = 'Shoes';
$lang['upload_accessories'] = 'Accessories';
$lang['upload_addgarment'] = 'Add Garment';
$lang['account_updatedata'] = 'Update Your Userdata';
$lang['account_newusername'] = 'New Username';
$lang['account_newpassword'] = 'New Password';
$lang['account_newemail'] = 'New Email';
$lang['account_updatesubmit'] = 'Update';
$lang['account_delete'] = 'Delete Your User Account';
$lang['account_deletesubmit'] = 'Delete';
$lang['canvas_savebutton'] = 'Save Canvas';
$lang['canvas_imagebutton'] = 'Export to PNG';
$lang['canvas_clearbutton'] = 'Clear Canvas';
$lang['canvas_tooltip_remove_white'] = 'Remove white background';
$lang['canvas_tooltip_remove_bg_color'] = 'Select background color to remove';

$lang['upload_addoutfit'] = 'Save outfit';

