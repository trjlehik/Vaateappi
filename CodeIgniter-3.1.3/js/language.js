//Nav palkissa olevaa kielipainiketta klikattaessa kutsutaan alla olevaa funktiota

$(function (){
    console.log("Loading Language.js");

$('#lang_english').click(function(){
    console.log("lang_english"); //ajaxilla lähetetään data controlleriin jossa tieto kielestä
        $.ajax({
            url: siteurl, //siteurl muuttuja on määritelty nav.php:ssä koska ulkoinen .js ei osaa käyttää codeigniterin url-apuria
            type: "POST",
            data: {language : "english"},
            success: function(data){
                window.location.reload(true);
            }
            
        });
});
$('#lang_italian').click(function(){
    
    console.log("lang_italian");
        $.ajax({
            url: siteurl,
            type: "POST",
            data: {language : "italian"},
            success: function(data){
                 window.location.reload(true);
            }
            
        });
});
});