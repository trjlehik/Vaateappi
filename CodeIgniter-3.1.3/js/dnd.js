$(function () {
  $('[data-toggle="tooltip"]').tooltip()
});



window.onload = function() {

    var imageGarmentIDs = [];

    fabric.Object.prototype.originX = 'right';
    fabric.Object.prototype.originY = 'top';


    fabric.Canvas.prototype.getAbsoluteCoords = function(object) {
        return {
          left: object.left + this._offset.left,
          top: object.top + this._offset.top
        };
    }


        //CANVAS SET UP
        var canvas = new fabric.Canvas('canvas', { backgroundColor : "#fff" });
        canvas.controlsAboveOverlay = true;

         // get references to the html canvas element & its context
        var canvasElement = document.getElementById('canvas');
        var ctx = canvasElement.getContext("2d");




        //GET CANVAS JSON OUTPUT
        var saveButton = document.getElementById('saveCanvasButton');

        saveButton.addEventListener('click', function(){

           console.log(JSON.stringify(canvas.toDatalessJSON()));
           console.log(JSON.stringify(imageGarmentIDs))

           $.ajax({
            url: "canvas_save",
            type: "POST",
            data: {
                JSON_String : JSON.stringify(canvas.toDatalessJSON()),
                image_ids: JSON.stringify(imageGarmentIDs)

            },
            success: function(data){
                $("#saved_message").show();
                setTimeout(function() { $("#saved_message").hide(); }, 5000);
                }
            });
        });

      /*  var imageCanvas = document.getElementById('c');
        var ctp = new CanvasToPNG('php/CanvasToPNG.php')
        ctp.setSaveButton('#saveToPNG', imageCanvas, 'myimage');*/

function dataURLtoBlob(dataurl) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while(n--){
        u8arr[n] = bstr.charCodeAt(n);
    }
    return new Blob([u8arr], {type:mime});
}


        //EXPORT CANVAS AS PNG
        $("#pngCanvasButton").click(function(){
            $('#outfitForm').modal();

            var dataurl = canvas.toDataURL({
                    format: 'png',
                    quality: 1
                })




            document.getElementById("previewOutfit").src = dataurl;
            document.getElementById("dataurl_string").value= dataurl;


    function form_to_json (selector) {
      var ary = $(selector).serializeArray();
      var obj = {};
      for (var a = 0; a < ary.length; a++) obj[ary[a].name] = ary[a].value;
      return obj;
    }

    outfitForm = document.querySelector("#outfitForm form");
    outfitForm.addEventListener('submit', function(e){


            e.preventDefault();

console.log({
                JSON_String : JSON.stringify(canvas.toDatalessJSON()),
                image_ids: JSON.stringify(imageGarmentIDs),
                formData: JSON.stringify(form_to_json(outfitForm))

            });

        $.ajax({
            url: "canvas_save",
            type: "POST",
            data: {
                JSON_String : JSON.stringify(canvas.toDatalessJSON()),
                image_ids: JSON.stringify(imageGarmentIDs),
                formData: JSON.stringify(form_to_json(outfitForm))

            },
            success: function(data){
                $('#outfitForm').modal('hide');
                $("#saved_message").show();
                setTimeout(function() { $("#saved_message").hide(); }, 5000);
                },
            error: function(data){
                console.log(data);
            }
            });
        });

    /*    if(!window.localStorage){alert("This function is not supported by your browser."); return;}
        window.open(canvas.toDataURL({
            format: 'png',
            quality: 1

        }));*/
        });

        //CLEAR CANVAS
        $("#clearCanvasButton").click(function(){
        canvas.clear();
        });

        //DRAG AND DROP
        /*
        NOTE: the start and end handlers are events for the <img> elements;
        the rest are bound to the canvas container.
        */


        function handleDragStart(e) {
            [].forEach.call(images, function (img) {
                img.classList.remove('img_dragging');
            });
            this.classList.add('img_dragging');
            console.log('HandleDragStart');

        }

        function handleDragOver(e) {
            if (e.preventDefault) {
                e.preventDefault();
            }

            e.dataTransfer.dropEffect = 'copy';
             console.log('HandleDragOver');
            return false;
        }

        function handleDragEnter(e) {
            this.classList.add('over');
            console.log('HandleDragEnter');
        }

        function handleDragLeave(e) {
            this.classList.remove('over');
            console.log('HandleDragLeave');
        }



        function hideImgTools() {
            var div;
            if(document.getElementById('canvas_imgTools') ) {

                div = document.getElementById('canvas_imgTools');
                div.parentNode.removeChild(div);
            }
        }

        function showImgTools(obj) {
        //GET
        var div;
        if(document.getElementById('canvas_imgTools') ) {
            div = document.getElementById('canvas_imgTools');
        } else { //CREATE TOOLS
            //wrapper
            div = document.createElement('div');
            div.setAttribute('id', 'canvas_imgTools');


            var deleteBtn = createButton('delete', obj);
            var rmWhiteBtn = createButton('rmWhite', obj);
    //        var rmColorBtn = createButton('rmColor', obj);

            div.appendChild(deleteBtn);
            div.appendChild(rmWhiteBtn);
     //       div.appendChild(rmColorBtn);

            document.body.appendChild(div);
        }

        var absCoords = canvas.getAbsoluteCoords(obj);
        div.style.left = (absCoords.left - div.offsetWidth) + 'px';
        div.style.top =   (absCoords.top) + 'px';

        }



    function createButton(action, obj){

        var imageHasWhitefilter = false;
        var imageHasColorfilter = false;

        for (var i = obj.filters.length; i--; ) {
            if(obj.filters[i].type == "RemoveWhite") { imageHasWhitefilter = true; }
            if(obj.filters[i].type == "RemoveColor") { imageHasColorfilter = true; }
            if (imageHasWhitefilter && imageHasColorfilter) break;
        }

        var icon, listener;
        var btnAttrList = {
                'type': 'button',
                'class': 'btn btn-default'
        };

        var button = document.createElement('button');

        switch (action) {
        case 'delete':
            icon = 'trash';
            listener = deleteButtonListener(obj);
            break;

        case 'rmWhite':
            var thisClass = imageHasWhitefilter ? 'btn btn-success' : 'btn btn-default';

            icon = 'scissors';
            btnAttrList = {
                'type': 'button',
                'class': thisClass,
                'data-placement': 'top',
                'title': rmWhite, /*global rmWhite*/ // defined in canvas.php
                'data-toggle': 'tooltip',
            };

            listener = removeWhiteListener(obj, button, imageHasWhitefilter);
            break;

    /*        case 'rmColor':
                var thisClass = imageHasColorfilter ? 'btn btn-success' : 'btn btn-default';
                booleanOfChoice = imageHasColorfilter;
                icon = 'adjust';
                btnAttrList = {
                    'type': 'button',
                    'class': thisClass,
                    'data-placement': 'top',
                    'title': rmBgColor, /*global rmBgColor // defined in canvas.php
                    'data-toggle': 'tooltip',
                };

                listener = removeColorListener(obj, button, imageHasColorfilter);
                break;*/
        }

        Object.entries(btnAttrList).forEach(
            ([key, value]) => {
                button.setAttribute(key, value);
                    if (value == 'tooltip') {
                        $(button).tooltip();
                    };
            }
        );

        var span = document.createElement('span');
        span.setAttribute('class', 'glyphicon glyphicon-'+icon);


        button.appendChild(span);
        button.addEventListener('click', function(e){
            listener(obj, button);

        });

        return button;
    }


    function deleteButtonListener() {
        return function(image){
            canvas.remove(image);
            //remove image, whose src matches with the one removed from canvas, from garment id (metadata)-array
            for (var i = imageGarmentIDs.length; i--; ) {
                if (imageGarmentIDs[i].src == image._element.currentSrc) {
                    imageGarmentIDs.splice(i, 1);
                    break;
                }
            }
        };
    }

    function removeWhiteListener(){
        return function(image, button){
            var imageHasThisfilter = false;


            for (var i = image.filters.length; i--; ) {
                  if(image.filters[i].type == "RemoveWhite") {
                      imageHasThisfilter = true;
                      image.filters.splice(i, 1);
                    break;
                  }
            }

                if (!imageHasThisfilter) {
                    var filter = new fabric.Image.filters.RemoveWhite({
                      threshold: 20,
                      distance: 60
                    });
                    image.filters.push(filter);
                    button.classList.remove('btn-default');
                    button.classList.add('btn-success');

                } else {
                    button.classList.remove('btn-success');
                    button.classList.add('btn-default');
                }


            image.applyFilters(canvas.renderAll.bind(canvas));
        }
    }


 function removeColorListener(){

        return function(image, button, imageHasThisfilter){

            /*

            filter-button clicked
            activate filter creation:
                -> button yellow; crosshair cursor
                -> click image -> create filter: push filter

            */
            var buttonActive = false;

            console.log(button);

            $(button).click(function(){
                buttonActive = true;
                button.classList.add('btn-warning');
                button.classList.remove('btn-default');
                            console.log('buttonclick', button);

            });

            if (buttonActive) {
                image.on('mouseover', function(){
                    image.hoverCursor = 'crosshair';
                });

                 image.on('mousedown', function(e) {
                        selectBGcolor(e, image);
                        imageHasThisfilter = true;
                    });

                image.on('deselected', function(){
                    image.hoverCursor = 'default';
                });


            }

/*            remove all these filters*/

                for (var i = image.filters.length; i--; ) {
                  if(image.filters[i].type == "RemoveColor") {
                    imageHasThisfilter = true;
                    image.filters.splice(i, 1);

                    break;
                  }

                };

                if (imageHasThisfilter) {
                    //set mouse cursor to crosshair
                    button.classList.add('btn-default');
                    button.classList.remove('btn-success');




                }

        }
    }

/*        Drop image onto canvas --> create fabric img object
*/
      function handleDrop(e) {

            var img = document.querySelector('#canvas-images img.img_dragging');

            imageGarmentIDs.push({
                'src': img.getAttribute('src'),
                'garment_id' : img.getAttribute('data-garment_id'),
                'image_id': img.getAttribute('data-image_id'),}
            );

            var canvasWidth = e.target.width;
            var canvasHeight = e.target.height;

           var scale = 1;

            var imageNeedsScaling = (canvasWidth * 0.7 < img.naturalWidth) || (canvasHeight * 0.7 < img.naturalHeight);
            if (imageNeedsScaling) {
                    scale = (0.5 * canvasHeight) / img.naturalHeight;
                }

            fabric.Image.fromURL(img.getAttribute('src'), function(thisimg) {

             var oImg = thisimg.set({ left: e.layerX, top: e.layerY }).scale(scale);

         oImg.hasControls = true;

            //hiding mid-segment controllers disables image scale distortion
            oImg.setControlsVisibility({
                 mt: false, //mid top
                 mb: false, //mid bottom
                 ml: false, //mid left
                 mr: false, //mid right
                 bl: true, //bottom left
                 br: true, //bottom right
                 tl: true, //top left
                 tr: true, //top right
                 mtr: true, //mid top-rotate
            });

            oImg.hasBorders = true;


            //event listeners handle showing/hiding tools for remove img from canvas & remove bg
            oImg.on('moving', function() { showImgTools(oImg);});
            oImg.on('modified', function() { showImgTools(oImg);});
            oImg.on('scaling', function() { showImgTools(oImg);});
            oImg.on('selected', function() { canvas.bringToFront(oImg); showImgTools(oImg);});
            oImg.on('deselected', function() { hideImgTools()});
            oImg.on('mouseout', function() { $('[data-toggle="tooltip"]').tooltip("hide"); });



          canvas.add(oImg).renderAll();
          canvas.setActiveObject(oImg);
        });


            console.log('HandleDrop');
            e.preventDefault();

            return false;
        }


        function handleDragEnd(e) {
            // this/e.target is the source node.
            [].forEach.call(images, function (img) {
                img.classList.remove('img_dragging');
            });
            console.log('HandleDragEnd');
        }
        console.log('Dnd.js End');


        if (Modernizr.draganddrop) {

           var images = document.querySelectorAll('#canvas-images img[draggable="true"]');
            [].forEach.call(images, function (img) {
                img.addEventListener('dragstart', handleDragStart, false);
                img.addEventListener('dragend', handleDragEnd, false);
            });

            var canvasContainer = document.getElementById("canvas-container");
            canvasContainer.addEventListener('dragenter', handleDragEnter, false);
            canvasContainer.addEventListener('dragover', handleDragOver, false);
            canvasContainer.addEventListener('dragleave', handleDragLeave, false);
            canvasContainer.addEventListener('drop', handleDrop, false);
        } else {
            //Fallback to Library method, TODO: Implement later
            alert("This browser doesn't support the HTML5 Drag and Drop API.");
        }



function selectBGcolor(e, image) {

      // get the current mouse position
      var mouse = canvas.getPointer(e.e);

      var x = parseInt(mouse.x);
      var y = parseInt(mouse.y);

      // get the color array for the pixel under the mouse
      var px = ctx.getImageData(x, y, 1, 1).data;

      // report that pixel data
      console.log('At [' + x + ' / ' + y + ']: Red/Green/Blue/Alpha = [' + px[0] + ' / ' + px[1] + ' / ' + px[2] + ' / ' + px[3] + ']');




        var filter = new fabric.Image.filters.RemoveColor({
          color: [px[0],px[1],px[2]]
        });

        image.filters.push(filter);
        image.applyFilters(canvas.renderAll.bind(canvas));




    };

}
