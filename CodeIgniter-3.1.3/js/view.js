 function showMyImage(fileInput) {

     var button = document.getElementById("imageSelectButton");
     button.textContent = " Switch image";
     button.setAttribute("class", "glyphicon glyphicon-repeat");


        var files = fileInput.files;
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            var imageType = /image.*/;
            if (!file.type.match(imageType)) {
                continue;
            }
            var div=document.getElementById("thumbnil");
            var img = div.getElementsByTagName("img").item(0);
            img.file = file;
            div.removeAttribute("class");
            var reader = new FileReader();
            reader.onload = (function(aImg) {
                return function(e) {
                    aImg.src = e.target.result;
                };
            })(img);
            reader.readAsDataURL(file);
        }



    var closeImage = document.querySelector("#thumbnil .close");
    closeImage.onclick = function(){
        var div=document.getElementById("thumbnil");
        var img = div.getElementsByTagName("img").item(0);

        div.setAttribute("class", "hidden");
        img.setAttribute("src", "");
        button.textContent = " Select image";
        button.setAttribute("class", "glyphicon glyphicon-plus");

    };

    }