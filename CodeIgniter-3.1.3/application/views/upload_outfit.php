
<!-- Modal -->
<div class="modal fade" id="outfitForm" tabindex="-1" role="dialog" aria-labelledby="outfitFormLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="outfitFormLabel"><?php echo lang('nav_createoutfit');?></h4>
      </div>
      <div class="modal-body">


<div >
<?php echo form_open_multipart('user_authentication/canvas_save');?>



<br />


<img id="previewOutfit" src=""/>


<input type="text" name="dataurl" id="dataurl_string" style="display: none">


 <br/>



 <br/>

<div class="form-group">
    <label for="garmName"><?php echo lang('upload_garmentname');?></label>
    <input class ="form-control" type="text" name="outfitName"/>
</div>

<div class="form-group">
    <label for="garmDscr"><?php echo lang('upload_description');?></label>
    <textarea class="form-control" name="outfitDscr" rows="3"></textarea>
</div>




<button type="submit" class="btn btn-primary">
          <span class="glyphicon glyphicon glyphicon-upload" aria-hidden="true"> <?php echo lang('upload_addoutfit');?></span>
        </button>





</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>




      </div>
    </div>



</form>
        </div>
  </div>
</div>



