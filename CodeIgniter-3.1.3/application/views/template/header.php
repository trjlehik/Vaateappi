<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>fashiONEapp</title>

    <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url();?>css/style.css">
    <script type = 'text/javascript' src = "<?php echo base_url();?>js/view.js"></script>

    <!--JQuery & Bootstrap ladataan CDN:stä, jos vaikka cachessa-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script type = 'text/javascript' src = "<?php echo base_url();?>js/language.js"></script>


  </head>
  <body>


      <!-- Navigaatio -->
      <?php
      $this->load->view('template/nav.php'); ?>

      <!-- Sisältöä tästä alaspäin, TODO Container CSS-->
      <div id="content" class = "container">