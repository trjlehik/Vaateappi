<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
    <!-- Logo ym. erikseen -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url();?>index.php">FASHIONEAPP</a> <!-- Superhieno logo tähän, h =< 65 px -->
    </div>

    <!-- Kaikki linkit, dropdownit jne. tähän, piilotetaan jos ei kirjautunut-->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

       <!--tsekataan onko kirjautunut-->
       <?php
            if (isset($this->session->userdata['logged_in'])) {
            $username = ($this->session->userdata['logged_in']['username']);

            ?>


      <ul class="nav navbar-nav">




        <li <?php if ($this->uri->uri_string() == 'upload') {echo "class='active'";} ?>  >
          <a href="<?php echo site_url("upload/index")?>"><?php echo lang('nav_addgarment');?></a>
        </li>
        <li  <?php if ($this->uri->uri_string() == 'user_authentication/canvas_show') {echo "class='active'";} ?>>
          <a href="<?php echo site_url("user_authentication/canvas_show")?>"><?php echo lang('nav_createoutfit');?></a>
        </li>
        <li  <?php if ($this->uri->uri_string() == 'imagecontroller') {echo "class='active'";} ?>>
          <a href="<?php echo site_url("imagecontroller")?>"><?php echo lang('nav_yourgarments');?></a>
        </li>



      </ul>

      <?php } //END IF SESSION_USERDATA..  ?>


      <div class="col-sm-3 ">
      <form class="navbar-form " role="search">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="<?php echo lang('nav_search');?>" name="q">
            <div class="input-group-btn">
                <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
            </div>
        </div>



      </form>
      </div>

      <ul class="nav navbar-nav navbar-right">

        <?php // -tsekataan onko kirjautunut, jos ei niin alasvetovalikon tilalla login painike
            if (isset($this->session->userdata['logged_in'])) {
            $username = ($this->session->userdata['logged_in']['username']);
            ?>


        <li class="dropdown <?php if ($this->uri->segment(1) == 'user_authentication') {echo 'active';} ?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
            <span> <?php echo $username; ?> </span>



            <span class="caret"></span></a>
          <ul class="dropdown-menu">

            <!--Show user profile-->
            <li><a href="<?php echo base_url();?>index.php/user_authentication/profile_show"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> <?php echo lang('nav_profile');?></a></li>

            <!--Show profile settings -->
            <li><a href="<?php echo base_url();?>index.php/user_authentication/admin_page_show"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> <?php echo lang('nav_accountsettings');?></a></li>





            <li role="separator" class="divider"></li>
            <!--Logout -->
            <li>
              <a href="<?php echo base_url();?>index.php/user_authentication/logout">
                 <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> <?php echo lang('nav_signout');?>
              </a>
           </li>
          </ul>
        </li>

         <?php } else {  //END IF SESSION_USERDATA.. ?>


              <li><a href="<?php echo base_url();?>index.php/user_authentication/user_login_show">
                <span class="glyphicon glyphicon-log-in"></span> <?php echo lang('nav_login');?>
              </a></li>

           <?php } ?>
           <!--kielivalinnat, painiketta klikattaessa kutsutaan language.js filen funktiota-->
           <li><a href="#" id="lang_italian">IT</a></li>
           <li><a href="#" id="lang_english">EN</a></li>
           <!--siteurl muuttuja määritellään tässä koska .js ei osaa käyttää url helperiä-->
           <script type="text/javascript">var siteurl = "<?php echo site_url('user_authentication/set_language');?>";</script>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>