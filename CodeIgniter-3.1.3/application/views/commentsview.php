<?php

//   http://bootsnipp.com/snippets/featured/user-comment-example
?>

<style>
    .thumbnail {
    padding:0px;
}
.panel {
	position:relative;
}

/** User comment, arrow to left, default bg */
.panel>.panel-heading:after,.panel>.panel-heading:before{
	position:absolute;
	top:11px;left:-16px;
	right:100%;
	width:0;
	height:0;
	display:block;
	content:" ";
	border-color:transparent;
	border-style:solid solid outset;
	pointer-events:none;
}
.panel>.panel-heading:after{
	border-width:7px;
	border-right-color:#f7f7f7;
	margin-top:1px;
	margin-left:2px;
}
.panel>.panel-heading:before{
	border-right-color:#ddd;
	border-width:8px;
}

/** Owner comment, arrow to right, default bg */

.panel.ownerComment>.panel-heading{
	z-index:2;
	background-color: #5ab6ff !important;
}

.panel.ownerComment>.panel-heading:after,.panel>.panel-heading:before{
	z-index:3;
	position:absolute;
	top:11px;
	left:100%;
	right:-1px;
	width:0;
	height:0;
	display:block;
	content:" ";
	border-color:transparent;
	border-style:outset solid ;
	pointer-events:none;
}
.panel.ownerComment>.panel-heading:after{
	border-width:7px;
	border-left-color:#5ab6ff;
	margin-top:1px;
	margin-left:1px;
}
.panel.ownerComment>.panel-heading:before{
	border-left-color:#5ab6ff;
	border-width:8px;
}

</style>



<?php defined('BASEPATH') OR exit('No direct script access allowed');



/**

 *
 * [id] => 2
    [content] => Such a lovely color! Would go well with a white cardigan
    [commentTimestamp] => 2017-03-23 14:01:31
    [fashionee_id] => 1
    [wearable_id] => 46
    [username] => Kati
*/


echo "<!--COMMENT FORM START-->";
echo"<div class='container col-md-8'>";
echo validation_errors();
echo"<br/>";
echo form_open('upload/add_comment');
echo form_textarea(array('name' => 'comment', 'class'=>'form-control', 'rows'=>2));
echo form_hidden('wearable_id', $wearable_id);
echo form_submit(array('name'=>'submit_comment', 'value'=>"Add comment!", 'class'=>'btn btn-default'));
echo form_close();
echo"</div>";
echo "<!--/COMMENT FORM END-->";

if (empty($query)) {
	echo "NO COMMENTS YET";
}

foreach ($query as $comment) {

	if ($comment['fashionee_id'] == $comment['wearable_owner_id']) {  //kommentti on vaatteen "omistajan" kirjoittama --> eri asettelu / css tyyli
		echo ownerComment($comment['username'], $comment['commentTimestamp'], $comment['content']);
	} else {
		echo visitorComment($comment['username'], $comment['commentTimestamp'], $comment['content']);
	}
} //endforeach comment



function visitorComment ($commentAuthor, $commentTimestamp, $commentText) {
return <<<HTML
<div class="row">
<div class="col-sm-1">

<img class="img-responsive img-circle user-photo" src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png">
</div><!-- /col-sm-1 -->

<div class="col-sm-5">
<div class="panel panel-default">
<div class="panel-heading">
<strong>
{$commentAuthor}
</strong>
<span class="text-muted">
{$commentTimestamp}
</span>
</div>
<div class="panel-body">
{$commentText}
</div><!-- /panel-body -->
</div><!-- /panel panel-default -->
</div><!-- /col-sm-5 -->
</div><!-- /row -->
HTML;
}

function ownerComment ($commentAuthor, $commentTimestamp, $commentText) {
return <<<HTML
<div class="row">

<div class="col-sm-5">
<div class="panel panel-default ownerComment">
<div class="panel-heading">
<strong>
{$commentAuthor}
</strong>
<span class="text-muted">
{$commentTimestamp}
</span>
</div>
<div class="panel-body">
{$commentText}
</div><!-- /panel-body -->
</div><!-- /panel panel-default -->

</div><!-- /col-sm-5 -->

<div class="col-sm-1">

<img class="img-responsive img-circle user-photo" src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png">
</div><!-- /col-sm-1 -->
</div><!-- /row -->
HTML;
}




?>

