<html>
<?php
if (isset($this->session->userdata['logged_in'])) {
$username = ($this->session->userdata['logged_in']['username']);
$email = ($this->session->userdata['logged_in']['email']);
$id = ($this->session->userdata['logged_in']['id']);
$joinTime = ($this->session->userdata['logged_in']['joinTime']);
} else {
header("location: login");
}
?>
<head>
<title>Profile Page</title>
<link rel="stylesheet" type="text/css" href="/css/style.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href="https://fonts.googleapis.com/css?family=Rubik+Mono+One" rel="stylesheet"> 

</head>
<body>
<div id="login">
<h1>Profile page</h1>
<p>Welcome to your own page <?php
echo $username
?> </p>
<p>Your username is <?php echo $username ?> </p>
<p>Your email is <?php echo $email ?> </p>
<p>You joined to the site <?php echo $joinTime ?> </p>
<p>Manage your user information at <a href="<?php echo base_url();?>index.php/user_authentication/admin_page_show"> <?php echo lang('nav_accountsettings');?></a></p>
</div>




</div>
<br/>
</body>
</html>