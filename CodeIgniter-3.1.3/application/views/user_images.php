    <script type="text/javascript">
           var baseurl = "<?php echo base_url();?>";
           var garmentname =" <?php echo lang('upload_garmentname');?> ";
           var description =" <?php echo lang('upload_description');?> ";
           var commentslink  ="View comments";
    </script>

    <script src="<?php echo base_url("/js/user_images.js")?>" ></script>

    <div class="row firstrow">
        <div class="well well-lg" id ="imagediv"> </div>
    </div>


    <div  id="images">

        <div class="row" id = "imagerow">


            <h2><?php echo lang('nav_yourgarments');?></h2>

                <span class="label label-info" id = "infobox"></span>
               <?php

               function isNotLocalFile($address) {

                    return (strpos($address, 'http') !== false || strpos($address, 'www') !== false || strpos($address, 'https')  !== false);

               }


                foreach($imgLocations as $img) {

                    if (isNotLocalFile($img->src)) {
                        echo ' <img src="'.$img->src.'" alt="cloth" height="100" width="100" class ="thumbnail" align="left" onclick= "getImagesrc(this.id)" id ='. $img->wearable_id.' > ';
                    } else {
                        echo ' <img src="'.base_url() .'uploads/'.$img->src .'" alt="cloth" height="100" width="100" class ="thumbnail" align="left" onclick= "getImagesrc(this.id)" id ='. $img->wearable_id.' > ';
                    }
                }
                ?>
        </div>
    </div>