

<?php echo $error;?>
<div id="login">
<?php echo form_open_multipart('upload/do_upload');?>



<br />


<label for="imageUpload" class="btn btn-primary">
    <span id="imageSelectButton" class="glyphicon glyphicon-plus" aria-hidden="true"> <?php echo lang('upload_selectimage');?></span></label>
<input type="file" name="userfile" id="imageUpload" accept="image/*" onchange="showMyImage(this)" style="display: none">


 <br/>

<div id="thumbnil" class="hidden"/>
    <button type="button" class="close" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
    <img src="" alt="image">
</div>


 <br/>

<div class="form-group">
    <label for="garmName"><?php echo lang('upload_garmentname');?></label>
    <input class ="form-control" type="text" name="garmName"/>
</div>

<div class="form-group">
    <label for="garmDscr"><?php echo lang('upload_description');?></label>
    <textarea class="form-control" name="garmDscr" rows="3"></textarea>
</div>

<div class="form-group">
    <p><?php echo lang('upload_category');?></p>
    <?php



    foreach ($category_rows as $category):?>

        <div class="form-check">
          <label class="form-check-label">
            <input class="form-check-input" type="radio" name="garment_category" value="<?php echo $category['id'];?>"id="<?php echo $category['id'];?>">
                <?php echo $category['category_name'];?>
            </label>
        </div>
            <?php endforeach;?>


</div>

<button type="submit" class="btn btn-primary"  value="upload" aria-label="Left Align">
  <span class="glyphicon glyphicon glyphicon-upload" aria-hidden="true"> <?php echo lang('upload_addgarment');?></span>
</button>


</div>

</form>

