<html>
 <?php
// if (isset($this->session->userdata['logged_in'])) {
// header("location: /index.php/user_authentication/user_login_process");
// }
// ?>
<head>
<title>Registration Form</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/css/style.css">


<!--<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:300|Raleway' rel='stylesheet' type='text/css'>-->
</head>
<body>
<div id="main">
<div id="login">
<h2><?php echo lang('register_form');?></h2>
<hr/>
<?php
echo "<div class='error_msg'>";
echo validation_errors();
echo "</div>";
echo form_open('user_authentication/new_user_registration');

echo form_label($this->lang->line('login_username'));
echo"<br/>";
$data = array(
'type' => 'text',
'name' => 'username',
'placeholder' => 'username'
);
echo form_input($data);
echo "<div class='error_msg'>";
if (isset($message_display)) {
echo $message_display;
}
echo "</div>";
echo"<br/>";
echo form_label($this->lang->line('register_email'));
echo"<br/>";
$data = array(
'type' => 'email',
'name' => 'email_value',
'placeholder' => 'email@example.com'
);
echo form_input($data);
echo"<br/>";
echo"<br/>";
echo form_label($this->lang->line('login_password'));
echo"<br/>";
$data = array(
'type' => 'password',
'name' => 'password',
'placeholder' => '********'
);
echo form_input($data);
echo"<br/>";
echo"<br/>";
echo form_label($this->lang->line('register_passwordconf'));
echo"<br/>";
$data = array(
'type' => 'password',
'name' => 'passconf',
'placeholder' => '********'
);
echo form_input($data);
echo"<br/>";
echo"<br/>";
echo form_submit('submit', $this->lang->line('register_signup'));
echo form_close();
?>
<a href='<?php echo base_url();?>index.php/user_authentication/user_login_show'><?php echo lang('login_link');?></a>
</div>
</div>
</body>
</html>