<script type="text/javascript">
    var rmWhite = "<?php echo lang('canvas_tooltip_remove_white');?>";
    var rmBgColor = "<?php echo lang('canvas_tooltip_remove_bg_color');?>";
</script>


    <!-- Fabrics & Modernizr JS -->
    <script src="<?php echo base_url();?>js/fabric.min.js"></script>
    <script src="<?php echo base_url();?>js/fabric.removeColor.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    <script src="<?php echo base_url();?>js/dnd.js"></script>


    <?php $this->load->view('thumbs', array("imgLocations" => $imgLocations)); ?>




    <div id="canvas-container">
        <canvas id="canvas" width="800" height="600"></canvas>
    </div>

    <div id="canvas-buttons">

        <!--SAVE CANVAS-->
        <button type="button" class="btn btn-success"  id="saveCanvasButton" data-toggle="tooltip" data-placement="bottom" title="Show JSON output on Browser console">
            <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> <?php echo lang('canvas_savebutton');?>
        </button><p id="saved_message" style="display:none">Canvas saved!</p>

        <!--CLEAR-->
        <button type="button" class="btn btn-warning"  id="clearCanvasButton" data-toggle="tooltip" data-placement="bottom" title="Clear canvas and remove all elements">
            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> <?php echo lang('canvas_clearbutton');?>
        </button>

        <!--SAVE IMAGE-->
        <button type="button" class="btn btn-default"  id="pngCanvasButton" data-toggle="tooltip" data-placement="bottom" title="Generate a PNG image of canvas">
            <span class="glyphicon glyphicon-picture" aria-hidden="true"></span> <?php echo lang('canvas_imagebutton');?>
        </button>

    </div>


