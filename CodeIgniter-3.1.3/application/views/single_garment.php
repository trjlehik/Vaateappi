
<?php defined('BASEPATH') OR exit('No direct script access allowed');


/*
    INPUT ARRAY $data
    	[id] => 14
        [timeAdded] => 2017-03-13 10:45:22
        [wearable_name] => Haglöfs windbreaker
        [description] => Something cool I recently bought
        [username] => Julia
        [category_id] => 2
        [category_name] => Shirt
        [src] => b1fd1634aeb38c7b8f5e1c00ab095c4c.jpg

*/

//format path for local image files;

if (strpos($src, 'dummyimage') === false) {  // $src doesn't contain string 'dummyimage'
	$src = base_url()."uploads/".$src;
};

?>

	<div id="wearable_<?php echo $id; ?>" class="panel col-md-8">

		<div class="page-header">
			<h1><?php echo $wearable_name; ?> 	<small><?php echo $timeAdded; ?></small></h1>
		</div>
	<p><span class="glyphicon glyphicon-user"></span> <?php echo $username; ?></p>

<?php if (isset($category_name)): ?>
	<p><span class="label label-default"><?php echo $category_name; ?></span></p>
<?php endif; ?>
	<p><?php echo $description; ?></p>


		<img class="col-md-8" src="<?php echo $src;?>"/>

	</div>
