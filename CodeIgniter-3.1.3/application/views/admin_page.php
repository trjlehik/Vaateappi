<html>
<?php
if (isset($this->session->userdata['logged_in'])) {
$username = ($this->session->userdata['logged_in']['username']);
$email = ($this->session->userdata['logged_in']['email']);
$id = ($this->session->userdata['logged_in']['id']);
} else {
header("location: login");
}
?>
<head>
<title>Admin Page</title>
<link rel="stylesheet" type="text/css" href="/css/style.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href="https://fonts.googleapis.com/css?family=Rubik+Mono+One" rel="stylesheet"> 

</head>
<body>

<!--Account update form-->

<div id="login">
<?php
echo form_open('user_authentication/update');
echo "<br/>";
echo $this->lang->line('account_updatedata');
echo "<br/>";
echo "<br/>";

echo form_label($this->lang->line('account_newusername'));
echo"<br/>";
$data = array(
'type' => 'text',
'name' => 'username',
'placeholder' => 'newusername'
);
echo form_input($data);
echo"<br/>";
echo"<br/>";
echo form_label($this->lang->line('account_newemail'));
echo"<br/>";
$data = array(
'type' => 'email',
'name' => 'email_value',
'placeholder' => 'email@example.com'
);
echo form_input($data);
echo"<br/>";
echo"<br/>";
echo form_label($this->lang->line('account_newpassword'));
echo"<br/>";
$data = array(
'type' => 'password',
'name' => 'password',
'placeholder' => '********'
);
echo form_input($data);
echo"<br/>";
echo"<br/>";
echo form_submit('submit', $this->lang->line('account_updatesubmit'));
echo form_close();
?>
</div>
<div id="login">

<!--Account delete form    -->
    
<?php echo form_open('user_authentication/delete', 'id="deleteform"'); ?>

<?php echo "<br/>";
echo $this->lang->line('account_delete'); 
echo "<br/>";
echo "<br/>";?>
<label><?php echo lang('login_username');?></label>
<input type="text" name="username" id="name" placeholder="username"/><br /><br />
<label><?php echo lang('login_password');?></label>
<input type="password" name="password" id="password" placeholder="**********"/><br/><br />
<input type="submit" value=<?php echo lang('account_deletesubmit');?> name="submit"/><br />



<?php echo form_close(); ?>

<!--Varmistetaan haluaako varmasti poistaa tilin-->

<script type="text/javascript">
    var el = document.getElementById('deleteform');

el.addEventListener('submit', function (event) { 
    if (!confirm('Do you really want to delete your account?')) { event.preventDefault(); } 
}, false);
</script>

</div>
<br/>
</body>
</html>