    <div id="canvas-images" class="row">
        <?php
        foreach($imgLocations as $imageRow){
            echo '<div class="col-lg-1 col-md-1 col-xs-2 thumb"> ';


            if($imageRow == "imagenotfound"){
              /* echo ' <img draggable="true" img src="'.base_url() .'placeholders/not_found.jpg"  height="100" width="100"    > ';*/
               echo ' <img src="'.base_url() .'placeholders/not_found.jpg" > ';
            }

            elseif (strpos($imageRow->src, 'http') !== false || strpos($imageRow->src, 'www') !== false || strpos($imageRow->src, 'https')  !== false) {
               /* echo ' <img draggable="true" img src="'.$imageRow->src.'" height="100" width="100"> ';*/
               echo ' <img src="'.$imageRow->src.'" data-garment_id="'.$imageRow->wearable_id.'"> ';
            }else {
                 /*echo ' <img draggable="true" img src="'.base_url() .'uploads/'.$imageRow->src .'" alt="cloth" height="100" width="100" > ';*/
                 echo ' <img draggable="true" src="'.base_url() .'uploads/'.$imageRow->src .'" alt="cloth" data-garment_id="'.$imageRow->wearable_id.'" data-image_id="'.$imageRow->id.'"> ';

            }

            echo '</div>';
        }

        ?>
    </div>
