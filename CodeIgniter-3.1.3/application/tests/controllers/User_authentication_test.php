<?php
//use PHPUnit\Framework\TestCase;
/**
 * Part of ci-phpunit-test
 *
 * @author     Tiia Lehikoinen
 *
 */

/**
 * @covers User_Authentication
 */
class User_Authentication_test extends TestCase
{
	//testataan index(), titlen avulla katsotaan että siirrytään oikealle sivulle
	public function test_index()
	{
		$output = $this->request('GET', 'user_authentication/index');
		$this->assertContains('<title>fashiONEapp</title>', $output);
	}

	public function test_method_404()
	{
		$this->request('GET', 'user_authentication/method_not_exist');
		$this->assertResponseCode(404);
	}

	public function test_APPPATH()
	{
		$actual = realpath(APPPATH);
		$expected = realpath(__DIR__ . '/../..');
		$this->assertEquals(
			$expected,
			$actual,
			'Your APPPATH seems to be wrong. Check your $application_folder in tests/Bootstrap.php'
		);
	}
	
	//testataan user_login_show(), titlen avulla katsotaan että siirrytään oikealle sivulle
	public function test_user_login_show()
	{
		$output = $this->request('GET', 'user_authentication/user_login_show');
		$this->assertContains('<title>Login Form</title>', $output);
	}
	
	//testataan user_registration_show(), titlen avulla katsotaan että siirrytään oikealle sivulle
	public function test_user_registration_show()
	{
		$output = $this->request('GET', 'user_authentication/user_registration_show');
		$this->assertContains('<title>Registration Form</title>', $output);
	}
	
	//testataan admin_page_show(), titlen avulla katsotaan että siirrytään oikealle sivulle
	public function test_admin_page_show()
	{
		$output = $this->request('GET', 'user_authentication/admin_page_show');
		$this->assertContains('<title>Admin Page</title>', $output);
	}
	
	//testataan profile_show(), titlen avulla katsotaan että siirrytään oikealle sivulle
	public function test_profile_show()
	{
		$output = $this->request('GET', 'user_authentication/profile_show');
		$this->assertContains('<title>Profile Page</title>', $output);
	}
}