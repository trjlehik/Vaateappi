<?php

//session_start(); //we need to start session in order to access it through CI

Class User_Authentication extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        // Load form helper library
        $this->load->helper('form');


        // Load form validation library
        $this->load->library('form_validation');

        // Load session library
        $this->load->library('session');

        try {
            // Load database, jos käyttäjää ei ole niin exception (jenkinsiä varten)
            $this->load->model('login_database');
            if (getenv('C9_USER') != "") {
                $this->load->database();
            }

        } catch (Exception $e) {
            throw new Exception($e);
        }

        //Load language helper library
        $this->load->helper('language');
        //Set session data 'language' and load language file
        if (empty($this->session->userdata['language'])) {
            $this->lang->load('ui', 'english');
        } else {
            $language = $this->session->userdata('language');
            $this->lang->load('ui', $language);
        }


    }

    // Show index page
    public function index()
    {
        $this->load->view('template/header');
        //$this->load->view('welcome_message');
        $this->load->view('front.php');
        $this->load->view('template/footer');
    }

    //Show login page
    public function user_login_show()
    {
        $this->load->view('template/header');
        $this->load->view('login_form');
        $this->load->view('template/footer');
    }

    // Show registration page
    public function user_registration_show()
    {
        $this->load->view('template/header');
        $this->load->view('registration_form');
        $this->load->view('template/footer');
    }

    // Show admin page
    public function admin_page_show()
    {
        $this->load->view('template/header');
        $this->load->view('admin_page');
        $this->load->view('template/footer');
    }

    //Show profile page
    public function profile_show()
    {
        $this->load->view('template/header');
        $this->load->view('profile');
        $this->load->view('template/footer');
    }

    // Validate and store registration data in database
    public function new_user_registration()
    {

        // // Check validation for user input in SignUp form
         $this->form_validation->set_rules('username', 'Username', 'required');
         $this->form_validation->set_rules('email_value', 'Email', 'required');
         $this->form_validation->set_rules('password', 'Password', 'required');
         $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');
         if ($this->form_validation->run() == FALSE) {
            $this->user_registration_show();
         } else {
            $data = array(
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email_value'),
                'password' => $this->input->post('password')
            );
            $result = $this->login_database->registration_insert($data);
            if ($result == TRUE) {
                //$data['message_display'] = 'Registration Successfully !';
                $this->user_login_show();
            } else {
                $data['message_display'] = 'Username already exist!';
                $this->load->view('registration_form', $data);
            }
        }
    }


    // Check for user login process
    public function user_login_process()
    {
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == FALSE) {
        // if(isset($this->session->userdata['logged_in'])){
        // $this->load->view('admin_page');
        // }else{
            $this->user_login_show();
        } else {
            $data = array(
                'username' => $this->input->post('username'),
                'password' => $this->input->post('password')
            );
            $result = $this->login_database->login($data);
            if ($result == TRUE) {

                $username = $this->input->post('username');
                $result = $this->login_database->read_user_information($username);
                if ($result != false) {
                    $session_data = array(
                        'id' => $result[0]->id,
                        'username' => $result[0]->username,
                        'email' => $result[0]->email,
                        'joinTime' => $result[0]->joinTime
                        );
                    // Add user data in session
                    $this->session->set_userdata('logged_in', $session_data);

                    $this->index();
                }
            } else {
                $data = array(
                  'error_message' => 'Invalid Username or Password'
                    );
                $this->load->view('login_form', $data);
            }
        }
    }

    // Logout
    public function logout()
    {
        // Removing session data
        $sess_array = array('username' => '');
        $this->session->unset_userdata('logged_in', $sess_array);

        $this->index();
    }


    //Update userdata
    public function update()
    {

        // Save username
        $username = $this->input->post('username');
        if ($username == "") {
            $username = $this->session->userdata['logged_in']['username'];
        }

        //Check empty keys
        $data = array(
            'username' => $this->input->post('username'),
            'email' => $this->input->post('email_value'),
            'password' => $this->input->post('password')
            );
        foreach ($data as $key => $value) {
            if (empty($value)) {
               unset($data[$key]);
            }
        }

        //Check empty array
        if (empty($data)) {
            $message['message_display'] = 'Nothing to update';
            $this->admin_page_show();
        } else {
            //Update info
            $result = $this->login_database->update_user_info($data);
            if ($result == TRUE) {
                //Get new userinfo
                $result = $this->login_database->read_user_information($username);
                if ($result != false) {
                    $session_data = array(
                    'id' => $result[0]->id,
                    'username' => $result[0]->username,
                    'email' => $result[0]->email
                    );
                // Add user data in session
                $this->session->set_userdata('logged_in', $session_data);

                //$message['message_display'] = 'User data updated!';
                $this->admin_page_show();
                }

            } else {
                //$message['message_display'] = 'Update failed';
                $this->admin_page_show();
            }
        }
    }

    //Delete user account
    public function delete()
    {
        $data = array(
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password')
            );
        $result = $this->login_database->delete_user($data);
        if ($result) {
            // Removing session data
            $sess_array = array('username' => '');
            $this->session->unset_userdata('logged_in', $sess_array);
            //$message['message_display'] = 'User account deleted';
            $this->index();
        } else {
            //$message['message_display'] = 'Invalid username or password';
            $this->admin_page_show();
        }
    }

    //READ POST INPUT FROM LANGUAGE.JS AND STORE TO USERDATA
    public function set_language()
    {
        $language = $this->input->post('language');
        $this->session->set_userdata(array('language' => $language));


        header('Content-type: application/json');
        echo json_encode(array("status" => TRUE));
    }

    //READ LANGUAGE FROM SESSION VARIABLE AND LOAD LANG FILE, NOT IN USE
    public function load_language()
    {

        $language = ($this->session->userdata('language'));
        $this->lang->load('ui', $language);

        header('Content-type: application/json');
        echo json_encode(array("status" => TRUE));
    }

    //Show canvas page
    public function canvas_show()
    {

        $this->load->library('session');
        $this->load->model('Fetch_images');

        $username = ($this->session->userdata['logged_in']['username']);
        $email = ($this->session->userdata['logged_in']['email']);
        $id = ($this->session->userdata['logged_in']['id']);


        $data ['imgLocations'] = $this-> Fetch_images-> fetch_all_images($id);



        $this->load->view('template/header');
        $this->load->view('canvas',$data);
        $this->load->view('upload_outfit');
        $this->load->view('template/footer');

    }


    public function save_image($data)
    {
        $this->load->helper('file');

        $imagedata = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data));

        $filename = uniqid('canvas_', true) . '.png';

        if ( ! write_file('./uploads/'.$filename, $imagedata)) {
            echo 'Unable to write the file';
        } else {
            echo 'File written!';
            echo '<img src="'.'./uploads/'.$filename.'">';
        }

        return $filename;
    }


    //READ CANVAS JSON STRING FROM POST DATA AND PASS ON TO CANVAS MODEL
    public function canvas_save()
    {
        $formData = json_decode($this->input->post('formData'), true);

        $imagedata= $formData['dataurl'];
        $filename = $this->save_image($imagedata);

        $ownerID = $this->session->userdata['logged_in']['id'];
        $json_String = $this->input->post('JSON_String');
        $image_ids = $this->input->post('image_ids');

        $formData['image_src'] = $filename;

        $this->load->model('image_upload');
        if($this->image_upload->canvasToDatabase($json_String, $ownerID, $image_ids, $formData)){

            echo json_encode(array("status" => TRUE));
        }else{
            echo json_encode(array("status" => FALSE));
        }
    }
}
?>