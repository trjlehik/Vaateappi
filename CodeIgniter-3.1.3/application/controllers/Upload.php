<?php

class Upload extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        /*helpers*/
        $this->load->database();
        $this->load->helper('form');
        $this->load->helper('language');
        $this->load->library('session');

        /*language*/
        if(empty($this->session->userdata['logged_in'])){
            $this->lang->load('ui', 'english');
        } else {
            $language = $this->session->userdata('language');
            $this->lang->load('ui', $language);
        }

        /*models*/
        $this->load->model('image_upload');
        $this->load->model('comments');

        /*libraries*/
        $this->load->library('form_validation');
        //$this->load->library('session');
    }

    public function index()
    {
        $category_rows = $this->image_upload->get_categories();

        $this->load->view('template/header');
        $this->load->view('upload_form', array('error' => '', 'category_rows' => $category_rows  ));
        $this->load->view('template/footer');

    }

    public function do_upload()
    {
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['encrypt_name']          = TRUE;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('userfile')) {

            $category_rows = $this->image_upload->get_categories();

            $error = array('error' => $this->upload->display_errors(), 'category_rows' => $category_rows );
            $this->load->view('upload_form', $error);

        } else {
            $uploadData = array(
                'image_src' => $this->upload->data('file_name'), //second optional parameter lets you run the data through the XSS filter
                'garmDscr' => $this->input->post('garmDscr', TRUE),
                'garmName' => $this->input->post('garmName', TRUE),
                'garment_category'  => $this->input->post('garment_category', TRUE),
                'fashionee' => $this->session->userdata['logged_in']['id']
            );

            $garment_id = $this->image_upload->upload($uploadData);
            $this->garment($garment_id);
        }
    }

    public function garment($id)
    {

        $data = $this->image_upload->select_wearable_by_id($id);


        $comments = $this->comments->get_comments($id);

        $this->load->view('template/header');
        $this->load->view('single_garment', $data);
        $this->load->view('commentsview', $comments);
        $this->load->view('template/footer');

    }

    public function add_comment()
    {
        $wearable_id = $this->input->post('wearable_id', TRUE);

        //tarkistetaan ettei kommenttikenttä ole tyhjä
        $this->form_validation->set_rules('comment', 'comment', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->garment($wearable_id);
        } else {

            $comment = $this->input->post('comment', TRUE);
            $wearable_id = $this->input->post('wearable_id', TRUE);
            $fashionee_id = ($this->session->userdata['logged_in']['id']);

            $this->comments->comment_wearable($wearable_id, $comment, $fashionee_id);
            $this->garment($wearable_id);
        }
    }

    public function edit_garment($id)
    {
            $data = $this->image_upload->select_garment_by_id($id);
            //TODO add something to differentiate edit from view

            $this->load->view('template/header');
            $this->load->view('single_garment', $data);
            $this->load->view('template/footer');
    }
    public function loadCommentsView($id)
    {
            /*$id= $this->input->get('id');*/
            $this->garment($id);

    }
}
