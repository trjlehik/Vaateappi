<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class imagecontroller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // Load session library
        $this->load->library('session');

        //Load helpers
        $this->load->helper('language');
        $this->load->helper(array('form', 'url'));

        // Load lang file
        if (empty($this->session->userdata['language'])) {
            $this->lang->load('ui', 'english');
        } else {
            $language = $this->session->userdata('language');
            $this->lang->load('ui', $language);
        }

       // Load database
       $this->load->database();

       // Load model
       $this->load->model('Fetch_images');

   }



   public function index()
   {
        $this->load->view('template/header');

        $username = ($this->session->userdata['logged_in']['username']);
        $email = ($this->session->userdata['logged_in']['email']);
        $id = ($this->session->userdata['logged_in']['id']);

        $data ['imgLocations'] = $this-> Fetch_images-> fetch_all_images($id);
        $data ['outfitLocations'] = $this-> Fetch_images-> fetch_all_images($id);

        $this->load->view('user_images', $data);

        $this->load->view('template/footer');
    }

     //Funktio (Ajaxia varten )joka lataa suuremman kuvan klikatusta garmentista ja kuvan tiedot

    public function getImageSource()
    {

        $id= $this->input->post('id');

        $this->load->model('Fetch_images');

        $image_src = $this-> Fetch_images-> fetch_image_location($id);
        $wearable_name = $this-> Fetch_images-> Search_WearableName_by_garmentID($id);
        $description = $this-> Fetch_images-> Search_WearableDescription_by_garmentID($id);

        echo json_encode(array($image_src, $wearable_name, $description));

    }

     //Funktio (Ajaxia varten )joka lataa kuvan nimen
    public function getImageInformation()
    {
        $id= $this->input->post('id');
        $this->load->model('Fetch_images');
        $data = $this-> Fetch_images-> Search_WearableName_by_garmentID($id);

        echo($data);

    }


}