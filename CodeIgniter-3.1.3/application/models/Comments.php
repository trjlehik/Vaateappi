<?php

Class Comments extends CI_Model {


    public function __construct() {
        parent::__construct();


   }
    /**
     * Retrieves comments for wearable = (outfit/garment)
     *
     * @param   int     wearable_id
     * @return  array
    */
    public function get_comments($wearable_id) {
        $this->load->database();

        $this->db->select('Comment.*, Fashionee.username, Wearable.fashionee_id as wearable_owner_id');
        $this->db->from('Comment');
        $this->db->where('wearable_id', $wearable_id);
        $this->db->join('Fashionee', 'Fashionee.id = Comment.fashionee_id');
        $this->db->join('Wearable', 'Wearable.id = Comment.wearable_id');
        $this->db->order_by('Comment.commentTimestamp', 'DESC');


        $query = array('wearable_id' => $wearable_id,
            'query' =>$this->db->get()->result_array());

        return $query;

    }

    public function comment_wearable($wearable_id, $comment, $fashionee_id) {
        $this->load->database();

        $commentData = array(
            'content'=> $comment,
            'fashionee_id'=> $fashionee_id,
            'wearable_id'=> $wearable_id);

        return $this->db->insert('Comment', $commentData);
    }

}