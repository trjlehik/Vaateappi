<?php

Class Image_upload extends CI_Model {


    public function __construct() {
        parent::__construct();


   }
    /**
     * Retrieves currently available categories and their ids from database;
     * to be used with the garment creation form or by admin interface
     *
     * @return array Each row contains 'id' and 'category_name'
    */
    public function get_categories() {
        $this->load->database();
        $query = $this->db->get('Category');
        return $query->result_array();
    }


    /**
     * An ugly giant piece of function to create new garments from uploaded images
     *
     * @return int    id for wearable that was just created
     */
    public function upload($uploadData)
    {

        //WEARABLE
        $wearableData = array(
            'wearable_name' => $uploadData['garmName'],
            'description' => $uploadData['garmDscr'],
            'fashionee_id' => $uploadData['fashionee']
            );
        $this->db->insert('Wearable', $wearableData);
        $wearable_id = $this->db->insert_id();

        //GARMENT
        $garmentData = array(
            'id' => $wearable_id,
            'category_id' => $uploadData['garment_category']
            );
        $this->db->insert('Garment', $garmentData);

        //IMAGE
        $imageData = array(
            'src' => $uploadData['image_src'],
            'wearable_id' => $wearable_id
            );
        $this->db->insert('Image', $imageData);


        return $wearable_id;
    }


    /**
     * Retrieve newest garments added by specified user
     * @param   int   $user_id
     * @param   int   $howMany    Limits number of results
     * @return array
    */
    public function select_garments_by_user($user_id, $howMany)
    {

        $this->db->select('Wearable.*, Image.src, Category.category_name, Fashionee.username');
        $this->db->from('Wearable');
        $this->db->join('Garment', 'Garment.id = Wearable.id');
        $this->db->join('Image', 'Image.wearable_id = Wearable.id');
        $this->db->join('Category', 'Category.id = Garment.category_id');
        $this->db->join('Fashionee', 'Fashionee.id = Wearable.fashionee_id');
        $this->db->where('fashionee_id', $user_id);
        $this->db->order_by('Wearable.timeAdded', 'DESC');
        $this->db->limit($howMany);

        if ($howMany == 1) {
            return $this->db->get()->result_array()[0];
        }

        return $this->db->get()->result_array();
    }

 /**
     * Retrieve single garment
     * @param   int   $garment_id
     * @return $arrayName = array('' => );
    */
    public function select_wearable_by_id($wearable_id)
    {
        if ($this->isOutfit($wearable_id)) {

            echo "yes";

            // this is an OUTFIT

            $this->db->select('Wearable.*, Image.src, Outfit.fabricObject, Fashionee.username');
            $this->db->from('Wearable');
            $this->db->join('Image', 'Image.wearable_id = Wearable.id');
            $this->db->join('Outfit', 'Outfit.id = Wearable.id');
            $this->db->join('Fashionee', 'Fashionee.id = Wearable.fashionee_id');
            $this->db->where('Wearable.id', $wearable_id);
            $this->db->order_by('Wearable.timeAdded', 'DESC');


        } else {

            // this is a GARMENT

            $this->db->select('Wearable.*, Image.src, Category.category_name, Fashionee.username');
            $this->db->from('Wearable');
            $this->db->join('Garment', 'Garment.id = Wearable.id');
            $this->db->join('Image', 'Image.wearable_id = Wearable.id');
            $this->db->join('Category', 'Category.id = Garment.category_id');
            $this->db->join('Fashionee', 'Fashionee.id = Wearable.fashionee_id');
            $this->db->where('Wearable.id', $wearable_id);
            $this->db->order_by('Wearable.timeAdded', 'DESC');

        }

        $result = $this->db->get()->result_array()[0];
        return $result;
    }


    private function isOutfit($wearable_id)
    {

        $query = $this->db->get_where('Outfit', array('id' => $wearable_id));


        if (empty($query->result_array())) {
            return false;
        }

        return true;

    }

    public function canvasToDatabase($fabricJSON, $ownerID, $image_ids, $formData)
    {

       $this->load->database();


       //WEARABLE
        $wearableData = array(
                'wearable_name' => $formData['outfitName'],
                'description' => $formData['outfitDscr'],
                'fashionee_id' => $ownerID
            );
        $this->db->insert('Wearable', $wearableData);
        $wearable_id = $this->db->insert_id();


       //OUTFIT
       $outfitData = array(
           'id'=>$wearable_id,
           'fabricObject' => $fabricJSON,
           );
       $this->db->insert('Outfit', $outfitData);


        //IMAGES_IN_OUTFIT
        $ids = json_decode($image_ids, true);

        $imageGarmentData = array();

        foreach ($ids as $image_in_outfit) {

            $idRow = array(
               'image_id' => $image_in_outfit['image_id'],
               'garment_id' => $image_in_outfit['garment_id'],
               'outfit_id'=> $wearable_id
               );

            if(!in_array($idRow, $imageGarmentData, true)){
                    array_push($imageGarmentData, $idRow);
                }
        }

        $this->db->insert_batch('image_of_garment_in_outfit', $imageGarmentData);
/*    TODO modify DB: garment_id --> wearable_id!!!
    foreign keys! upload/controller/views etc etc
*/
        //OUTFIT IMAGE
        $imageData = array(
            'src' => $formData['image_src'],
            'wearable_id' => $wearable_id
            );
        $this->db->insert('Image', $imageData);



       return true;
   }





}
?>