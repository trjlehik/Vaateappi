
<?php

Class Login_Database extends CI_Model  { //

// Query to check whether username already exist or not
public function check_username($data) {

if (!empty($data['userName'])) {
$condition = "username =" . "'" . $data['username'] . "'";
$this->db->select('*');
$this->db->from('Fashionee');
$this->db->where($condition);
$this->db->limit(1);
$query = $this->db->get();
return ($query->num_rows());
}
else {
    return 0;
}
}

// Insert registration data in database
public function registration_insert($data) {

// Query to check whether username already exist or not

if ($this-> check_username($data) == 0) {

// Query to insert data in database
$this->db->insert('Fashionee', $data);
if ($this->db->affected_rows() > 0) {
return true;
}
} else {
return false;
}
}

// Read data using username and password
public function login($data) {

$condition = "username =" . "'" . $data['username'] . "' AND " . "password =" . "'" . $data['password'] . "'";
$this->db->select('*');
$this->db->from('Fashionee');
$this->db->where($condition);
$this->db->limit(1);
$query = $this->db->get();

if ($query->num_rows() == 1) {
return true;
} else {
return false;
}
}

// Read data from database to show data in admin page
public function read_user_information($username) {

$condition = "username =" . "'" . $username . "'";
$this->db->select('id, username, email, joinTime');
$this->db->from('Fashionee');
$this->db->where($condition);
$this->db->limit(1);
$query = $this->db->get();

if ($query->num_rows() == 1) {
return $query->result();
} else {
return false;
}
}
// Update userdata at database
public function update_user_info($data) {

// Query to check whether username already exist or not
if ($this-> check_username($data) == 0) {
$condition = "id =" . "'" . $this->session->userdata['logged_in']['id'] . "'";
$this->db->where($condition);

$this->db->update('Fashionee', $data);

if ($this->db->affected_rows() > 0) {
    return true;

} else {
return false;
}

} else {
return false;
}
}

//delete user data at database
public function delete_user($data) {

//check correct username and password
if ($this ->login($data)) {
    $condition = "username =" . "'" . $data['username'] . "'";

    //create empty user data
    $emptydata = array(
    'username' => '',
    'email' => '',
    'password' => $data['password']
);
    $this->db->where($condition);

    $this->db->update('Fashionee', $emptydata);

    if ($this->db->affected_rows() > 0) {
        return true;

    } else {
        return false;
    }
}
}
}

?>