<?php

Class fetch_images extends CI_Model {


    public function __construct()
    {
        parent::__construct();
    }

    public function Search_user_entities($user_id){

        $this->db->select('*');
        $this->db->from('Wearable');
        $this->db->where('fashionee_id', $user_id);


        return $this->db->get()->result_array();
    }


     public function Search_WearableName_by_garmentID($garment_id)
     {

         $query= $this->db->query("SELECT wearable_name FROM Wearable
                                    WHERE id = (". $garment_id . ")"  );
         $row = $query->row()->wearable_name;

         return $row;
    }

    public function Search_WearableDescription_by_garmentID($garment_id)
    {

        $query= $this->db->query("SELECT description FROM Wearable
                                WHERE id = (". $garment_id . ")"  );
        $row = $query->row()->description;

        return $row;
    }



     public function fetch_image_location($garment_id) {
        //$entityArray = $this->Search_user_entities($user_id);

        $query;
        $srcArray = array();

        $query= $this->db->query("SELECT src FROM Image
                                WHERE wearable_id = (". $garment_id . ")"  );
        $row = $query->row()->src;

        return $row;

      //return $this->db->get()->result_array();
     }


    public function fetch_all_images($user_id)
   {

        $entityArray = $this->Search_user_entities($user_id);

        $query;
        $srcArray = array();

        foreach ($entityArray as $l) {
            $query= $this->db->query("SELECT * FROM Image
                                WHERE wearable_id = (". $l['id'] . ")"  );


            if (is_null($query->row()) == true ) {
              /* array_push($srcArray, "imagenotfound" );*/
            } else {
              array_push($srcArray, $query->row() );
            }

        }
        // print_r($srcArray);


        return $srcArray;

    }
}
