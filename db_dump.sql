SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS Fashionee;
DROP TABLE IF EXISTS Image;
DROP TABLE IF EXISTS Wearable;
DROP TABLE IF EXISTS Garment;
DROP TABLE IF EXISTS Outfit;
DROP TABLE IF EXISTS Comment;
DROP TABLE IF EXISTS Entity;
DROP TABLE IF EXISTS Tag;
DROP TABLE IF EXISTS Category;
DROP TABLE IF EXISTS tag_in_wearable;
DROP TABLE IF EXISTS tag_in_entity;
DROP TABLE IF EXISTS image_of_garment_in_outfit;
SET FOREIGN_KEY_CHECKS = 1;

CREATE TABLE Fashionee
(
  id INT NOT NULL AUTO_INCREMENT,
  username VARCHAR(25) NOT NULL,
  joinTime TIMESTAMP,
  email VARCHAR(50) NOT NULL,
  password VARCHAR(50) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE Tag
(
  id INT NOT NULL AUTO_INCREMENT,
  tag_name VARCHAR(25) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE Wearable
(
  id INT NOT NULL AUTO_INCREMENT,
  timeAdded TIMESTAMP  NOT NULL,
  wearable_name TEXT,
  description TEXT,
  fashionee_id INT NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (fashionee_id) REFERENCES Fashionee(id)
);

CREATE TABLE tag_in_wearable
(
  tag_id INT NOT NULL,
  wearable_id INT NOT NULL,
  PRIMARY KEY (tag_id, wearable_id),
  FOREIGN KEY (tag_id) REFERENCES Tag(id),
  FOREIGN KEY (wearable_id) REFERENCES Wearable(id)
);

CREATE TABLE Category
(
  id INT NOT NULL AUTO_INCREMENT,
  category_name VARCHAR(50) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE Comment
(
  id INT NOT NULL AUTO_INCREMENT,
  content VARCHAR(255) NOT NULL,
  commentTimestamp TIMESTAMP NOT NULL,
  fashionee_id INT NOT NULL,
  wearable_id INT NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (fashionee_id) REFERENCES Fashionee(id),
  FOREIGN KEY (wearable_id) REFERENCES Wearable(id)
);

CREATE TABLE Garment
(
  id INT NOT NULL,
  category_id INT NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (id) REFERENCES Wearable(id),
  FOREIGN KEY (category_id) REFERENCES Category(id)
);

CREATE TABLE Image
(
  id INT NOT NULL AUTO_INCREMENT,
  src VARCHAR(100) NOT NULL,
  wearable_id INT NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (wearable_id) REFERENCES Wearable(id)
);

CREATE TABLE Outfit
(
  id INT NOT NULL,
  fabricObject TEXT NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (id) REFERENCES Wearable(id)
);


CREATE TABLE image_of_garment_in_outfit
(
  image_id INT NOT NULL,
  garment_id INT NOT NULL,
  outfit_id INT NOT NULL,
  PRIMARY KEY (image_id, garment_id, outfit_id),
  FOREIGN KEY (image_id) REFERENCES Image(id),
  FOREIGN KEY (garment_id) REFERENCES Garment(id),
  FOREIGN KEY (outfit_id) REFERENCES Outfit(id)
);


SET FOREIGN_KEY_CHECKS = 0;
DELETE FROM `c9`.`Category` WHERE `Category`.`id` = 1;
DELETE FROM `c9`.`Category` WHERE `Category`.`id` = 2;
DELETE FROM `c9`.`Category` WHERE `Category`.`id` = 3;
DELETE FROM `c9`.`Category` WHERE `Category`.`id` = 4;
DELETE FROM `c9`.`Category` WHERE `Category`.`id` = 5;
DELETE FROM `c9`.`Category` WHERE `Category`.`id` = 6;
DELETE FROM `c9`.`Category` WHERE `Category`.`id` = 7;
SET FOREIGN_KEY_CHECKS = 1;









/** ----------------------------------------------------------------
 ------------------------- DATA -----------------------------------
-------------------------------------------------------------------*/




/** ----------------------- 7 CATEGORIES ---------------------------------*/

INSERT INTO `Category` VALUES (1,'Legwear'),(2,'Jacket'),(3,'Shirt'),(4,'Knit'),(5,'Dress'),(6,'Shoes'),(7,'Accessories');



/** ----------------------- 10 FASHIONEES ---------------------------------*/


insert into Fashionee (id, username, joinTime, email, password) values (1, 'Sean', '2016-12-06 03:16:01', 'ssanchez0@dedecms.com', 'Sean');
insert into Fashionee (id, username, joinTime, email, password) values (2, 'Doris', '2016-07-11 17:48:07', 'dwilliamson1@lulu.com', 'Doris');
insert into Fashionee (id, username, joinTime, email, password) values (3, 'Julie', '2016-10-05 19:44:43', 'jgordon2@youtu.be', 'Julie');
insert into Fashionee (id, username, joinTime, email, password) values (4, 'Frances', '2016-09-09 15:50:55', 'fparker3@angelfire.com', 'Frances');
insert into Fashionee (id, username, joinTime, email, password) values (5, 'Deborah', '2017-03-17 11:48:13', 'dmcdonald4@google.de', 'Deborah');
insert into Fashionee (id, username, joinTime, email, password) values (6, 'Anna', '2016-11-12 05:55:19', 'aporter5@networksolutions.com', 'Anna');
insert into Fashionee (id, username, joinTime, email, password) values (7, 'Ruby', '2016-10-05 08:49:39', 'rlawrence6@eepurl.com', 'Ruby');
insert into Fashionee (id, username, joinTime, email, password) values (8, 'Julia', '2017-03-16 03:46:37', 'jowens7@buzzfeed.com', 'Julia');
insert into Fashionee (id, username, joinTime, email, password) values (9, 'Gary', '2016-06-29 01:33:22', 'gwright8@springer.com', 'Gary');
insert into Fashionee (id, username, joinTime, email, password) values (10, 'Joseph', '2016-09-26 04:23:24', 'jking9@so-net.ne.jp', 'Joseph');

/** ----------------------- 40 WEARABLES ---------------------------------*/


insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (1, '2016-12-16 09:44:06', 'sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit', 'Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus.', 10);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (2, '2017-01-22 20:22:27', 'sed accumsan felis ut at dolor quis odio consequat varius integer ac leo pellentesque ultrices', 'Praesent lectus. Vestibulum quam sapien, varius ut, blandit non, interdum in, ante.', 2);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (3, '2016-07-02 01:28:02', 'sed tincidunt eu felis fusce posuere felis sed lacus morbi sem mauris laoreet ut rhoncus', 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum.', 10);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (4, '2016-03-26 00:01:25', 'nec euismod scelerisque quam turpis adipiscing lorem vitae mattis nibh ligula', 'Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo.', 9);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (5, '2016-04-28 11:50:42', 'at ipsum ac tellus semper interdum mauris ullamcorper purus', 'Etiam faucibus cursus urna. Ut tellus.', 4);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (6, '2016-06-29 15:28:02', 'quis orci nullam molestie nibh in lectus pellentesque at nulla suspendisse potenti cras in', 'Pellentesque eget nunc.', 9);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (7, '2016-07-27 21:53:47', 'vulputate justo in blandit ultrices enim lorem', 'Phasellus in felis.', 5);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (8, '2017-02-27 00:49:41', 'vel accumsan tellus nisi eu orci mauris lacinia sapien quis', 'Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 6);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (9, '2016-03-28 20:43:53', 'pellentesque eget nunc', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit.', 7);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (10, '2016-08-02 15:12:49', 'vel accumsan tellus', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', 10);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (11, '2017-03-13 04:45:57', 'felis sed interdum venenatis turpis enim blandit mi in porttitor pede justo eu massa donec', 'Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', 9);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (12, '2016-07-29 14:14:21', 'eros viverra', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', 6);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (13, '2016-09-13 15:08:02', 'eros viverra eget congue eget', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', 8);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (14, '2016-09-03 14:46:03', 'enim lorem ipsum dolor sit', 'Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula.', 7);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (15, '2017-01-05 08:57:01', 'lectus suspendisse potenti in eleifend quam a odio in hac habitasse platea dictumst maecenas ut', 'Duis bibendum.', 4);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (16, '2016-07-07 23:54:29', 'maecenas rhoncus aliquam lacus morbi quis tortor id nulla ultrices aliquet maecenas leo', 'Fusce consequat. Nulla nisl.', 5);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (17, '2016-10-27 02:10:26', 'tristique fusce congue diam id ornare imperdiet sapien urna pretium nisl ut', 'Nullam varius.', 9);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (18, '2017-02-06 07:59:09', 'lacinia eget tincidunt', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 5);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (19, '2016-08-29 02:56:02', 'nisl nunc nisl duis bibendum felis sed interdum venenatis turpis enim blandit mi in porttitor', 'Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', 1);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (20, '2017-03-24 11:56:36', 'luctus ultricies eu nibh quisque id justo sit', 'Vivamus tortor. Duis mattis egestas metus.', 2);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (21, '2016-12-25 12:30:12', 'lorem integer tincidunt ante vel ipsum praesent blandit lacinia', 'Nulla nisl. Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa.', 7);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (22, '2016-05-22 01:01:16', 'phasellus id sapien', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum.', 9);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (23, '2016-11-29 01:27:03', 'arcu libero rutrum', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', 3);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (24, '2017-02-10 15:05:39', 'habitasse platea', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est.', 8);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (25, '2016-10-14 08:49:18', 'quis justo maecenas rhoncus aliquam', 'Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', 6);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (26, '2016-07-24 10:16:25', 'nibh ligula nec sem duis aliquam convallis nunc', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis.', 9);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (27, '2016-11-21 23:52:17', 'rutrum nulla nunc purus phasellus in felis', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros.', 10);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (28, '2016-07-14 02:21:22', 'luctus cum sociis natoque penatibus et magnis dis parturient montes nascetur', 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', 3);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (29, '2017-03-17 08:12:06', 'consequat ut nulla sed accumsan felis ut', 'Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 10);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (30, '2016-11-17 22:57:47', 'faucibus orci luctus et ultrices posuere cubilia curae duis faucibus accumsan odio curabitur', 'Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue.', 5);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (31, '2016-04-17 19:03:17', 'sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit at', 'Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum.', 8);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (32, '2016-05-20 01:34:07', 'ac enim in tempor', 'Nulla tempus.', 6);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (33, '2016-12-27 04:54:44', 'dolor quis odio consequat varius integer ac', 'Sed ante.', 6);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (34, '2017-01-26 09:18:34', 'ultrices enim lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum mauris', 'Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo.', 5);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (35, '2016-07-28 08:44:32', 'maecenas tristique est et tempus semper est quam pharetra magna ac consequat', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', 5);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (36, '2016-12-27 01:11:32', 'donec quis orci eget orci vehicula condimentum curabitur in', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo.', 9);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (37, '2016-04-01 14:04:10', 'enim leo rhoncus sed vestibulum sit amet cursus id turpis integer aliquet massa id', 'Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis.', 5);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (38, '2017-03-21 01:46:12', 'sit amet justo morbi ut odio cras mi', 'Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', 2);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (39, '2016-12-31 01:09:16', 'consequat metus sapien ut nunc vestibulum ante ipsum primis in', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', 1);
insert into Wearable (id, timeAdded, wearable_name, description, fashionee_id) values (40, '2016-10-12 08:01:32', 'ultrices posuere cubilia curae nulla', 'Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat.', 8);



/** ----------------------- 20 GARMENTS ---------------------------------*/

insert into Garment (id, category_id) values (1, 7);
insert into Garment (id, category_id) values (2, 1);
insert into Garment (id, category_id) values (3, 1);
insert into Garment (id, category_id) values (4, 3);
insert into Garment (id, category_id) values (5, 5);
insert into Garment (id, category_id) values (6, 2);
insert into Garment (id, category_id) values (7, 4);
insert into Garment (id, category_id) values (8, 3);
insert into Garment (id, category_id) values (9, 5);
insert into Garment (id, category_id) values (10, 3);
insert into Garment (id, category_id) values (11, 7);
insert into Garment (id, category_id) values (12, 7);
insert into Garment (id, category_id) values (13, 1);
insert into Garment (id, category_id) values (14, 2);
insert into Garment (id, category_id) values (15, 3);
insert into Garment (id, category_id) values (16, 6);
insert into Garment (id, category_id) values (17, 3);
insert into Garment (id, category_id) values (18, 3);
insert into Garment (id, category_id) values (19, 4);
insert into Garment (id, category_id) values (20, 5);




/** ----------------------- 20 IMAGES ---------------------------------*/


insert into Image (id, wearable_id, src) values (1, 1, 'http://dummyimage.com/500x500.jpg/ff4444/ffffff');
insert into Image (id, wearable_id, src) values (2, 2, 'http://dummyimage.com/500x500.bmp/cc0000/ffffff');
insert into Image (id, wearable_id, src) values (3, 3, 'http://dummyimage.com/500x500.png/cc0000/ffffff');
insert into Image (id, wearable_id, src) values (4, 4, 'http://dummyimage.com/500x500.png/dddddd/000000');
insert into Image (id, wearable_id, src) values (5, 5, 'http://dummyimage.com/500x500.png/ff4444/ffffff');
insert into Image (id, wearable_id, src) values (6, 6, 'http://dummyimage.com/500x500.bmp/ff4444/ffffff');
insert into Image (id, wearable_id, src) values (7, 7, 'http://dummyimage.com/500x500.jpg/ff4444/ffffff');
insert into Image (id, wearable_id, src) values (8, 8, 'http://dummyimage.com/500x500.jpg/5fa2dd/ffffff');
insert into Image (id, wearable_id, src) values (9, 9, 'http://dummyimage.com/500x500.png/dddddd/000000');
insert into Image (id, wearable_id, src) values (10, 10, 'http://dummyimage.com/500x500.jpg/dddddd/000000');
insert into Image (id, wearable_id, src) values (11, 11, 'http://dummyimage.com/500x500.bmp/cc0000/ffffff');
insert into Image (id, wearable_id, src) values (12, 12, 'http://dummyimage.com/500x500.jpg/cc0000/ffffff');
insert into Image (id, wearable_id, src) values (13, 13, 'http://dummyimage.com/500x500.jpg/cc0000/ffffff');
insert into Image (id, wearable_id, src) values (14, 14, 'http://dummyimage.com/500x500.bmp/cc0000/ffffff');
insert into Image (id, wearable_id, src) values (15, 15, 'http://dummyimage.com/500x500.jpg/ff4444/ffffff');
insert into Image (id, wearable_id, src) values (16, 16, 'http://dummyimage.com/500x500.png/cc0000/ffffff');
insert into Image (id, wearable_id, src) values (17, 17, 'http://dummyimage.com/500x500.jpg/cc0000/ffffff');
insert into Image (id, wearable_id, src) values (18, 18, 'http://dummyimage.com/500x500.jpg/cc0000/ffffff');
insert into Image (id, wearable_id, src) values (19, 19, 'http://dummyimage.com/500x500.bmp/ff4444/ffffff');
insert into Image (id, wearable_id, src) values (20, 20, 'http://dummyimage.com/500x500.bmp/cc0000/ffffff');




/** ----------------------- 100 COMMENTS ---------------------------------*/

insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (1, 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', '2016-07-26 11:50:03', 7, 26);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (2, 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', '2016-04-26 10:46:22', 4, 38);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (3, 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', '2016-05-20 02:20:29', 2, 7);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (4, 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', '2017-02-10 02:59:11', 8, 13);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (5, 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', '2016-12-29 16:55:18', 10, 21);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (6, 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', '2017-02-02 06:24:31', 9, 38);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (7, 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', '2016-10-26 23:26:27', 9, 1);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (8, 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', '2016-06-28 23:55:13', 6, 34);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (9, 'Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', '2016-07-02 07:48:35', 9, 19);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (10, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.

Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.', '2017-02-01 07:39:17', 8, 29);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (11, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.', '2016-10-10 13:28:35', 1, 12);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (12, 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.

Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', '2016-12-12 12:37:57', 7, 33);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (13, 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', '2017-01-20 14:49:47', 6, 28);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (14, 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', '2017-02-22 23:27:02', 1, 9);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (15, 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', '2016-07-21 13:08:14', 2, 5);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (16, 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', '2017-01-11 01:23:12', 4, 15);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (17, 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', '2017-03-13 09:24:42', 8, 37);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (18, 'Fusce consequat. Nulla nisl. Nunc nisl.', '2016-04-13 01:11:34', 8, 3);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (19, 'Sed ante. Vivamus tortor. Duis mattis egestas metus.', '2016-06-26 05:26:25', 1, 25);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (20, 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', '2017-02-06 13:55:42', 1, 15);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (21, 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.', '2016-10-19 16:18:47', 8, 29);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (22, 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', '2016-03-30 18:09:26', 10, 7);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (23, 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', '2016-05-27 16:56:31', 3, 37);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (24, 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', '2017-03-09 11:00:45', 9, 16);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (25, 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.

Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', '2016-08-01 12:36:09', 8, 10);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (26, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.', '2016-12-22 09:22:48', 7, 29);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (27, 'Phasellus in felis. Donec semper sapien a libero. Nam dui.', '2016-03-31 20:01:38', 2, 23);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (28, 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', '2017-01-10 20:58:20', 10, 22);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (29, 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', '2016-11-25 13:40:31', 3, 2);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (30, 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', '2016-07-18 04:08:40', 10, 39);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (31, 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', '2016-09-16 07:09:07', 10, 26);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (32, 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', '2017-01-09 21:08:21', 4, 32);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (33, 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', '2016-05-10 13:23:21', 5, 10);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (34, 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', '2016-12-30 01:01:22', 2, 12);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (35, 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2016-05-30 20:55:52', 4, 8);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (36, 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', '2017-01-10 22:05:33', 3, 25);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (37, 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '2016-08-27 06:52:59', 7, 2);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (38, 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', '2016-11-17 04:05:37', 4, 40);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (39, 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', '2016-09-22 20:41:32', 3, 9);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (40, 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', '2017-03-20 01:58:56', 2, 13);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (41, 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.

Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', '2016-11-21 12:18:12', 1, 27);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (42, 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.

Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', '2016-09-27 17:33:31', 1, 17);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (43, 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.

Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '2016-04-25 13:16:21', 9, 8);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (44, 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', '2016-08-17 17:45:21', 4, 35);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (45, 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', '2017-01-14 00:28:23', 9, 39);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (46, 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', '2016-07-19 04:26:11', 7, 5);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (47, 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', '2016-08-26 05:42:12', 10, 38);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (48, 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.

Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', '2016-06-06 13:00:07', 9, 36);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (49, 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', '2017-02-01 14:54:15', 1, 13);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (50, 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', '2016-11-26 13:36:58', 4, 20);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (51, 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', '2017-01-26 01:33:47', 10, 11);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (52, 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', '2016-09-25 03:55:18', 9, 34);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (53, 'Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', '2016-07-31 22:53:39', 6, 38);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (54, 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', '2016-05-19 06:25:51', 4, 5);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (55, 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', '2016-05-19 22:42:34', 5, 32);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (56, 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.

Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.', '2016-11-21 23:49:48', 8, 24);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (57, 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', '2016-06-14 04:25:10', 2, 6);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (58, 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.

Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', '2016-06-10 00:39:28', 9, 18);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (59, 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', '2016-11-30 08:53:03', 1, 23);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (60, 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', '2016-12-04 04:18:35', 5, 40);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (61, 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', '2016-05-30 14:58:29', 10, 1);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (62, 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.

Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', '2016-04-20 16:18:16', 7, 9);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (63, 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2016-10-05 13:52:35', 2, 12);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (64, 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', '2017-02-02 13:55:25', 5, 19);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (65, 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', '2017-01-17 14:36:24', 4, 32);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (66, 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.

Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', '2016-11-08 09:05:06', 7, 17);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (67, 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.', '2016-07-20 19:30:58', 7, 4);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (68, 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.', '2016-12-28 11:19:50', 6, 39);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (69, 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.

Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', '2017-01-04 15:38:44', 10, 28);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (70, 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', '2016-10-13 18:55:38', 5, 10);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (71, 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', '2016-04-09 22:41:39', 4, 27);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (72, 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', '2016-11-16 00:05:45', 3, 31);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (73, 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.

Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', '2017-03-08 11:18:31', 9, 24);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (74, 'Phasellus in felis. Donec semper sapien a libero. Nam dui.', '2016-12-01 20:41:55', 1, 6);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (75, 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', '2016-04-13 12:08:55', 4, 28);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (76, 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.', '2016-11-07 19:34:37', 3, 17);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (77, 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', '2016-09-03 03:07:28', 1, 11);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (78, 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', '2017-03-10 19:32:59', 9, 16);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (79, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.

Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.', '2017-03-24 03:17:00', 1, 4);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (80, 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', '2016-06-21 21:48:51', 1, 28);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (81, 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', '2016-10-07 18:57:25', 7, 5);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (82, 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', '2016-09-07 01:44:22', 7, 21);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (83, 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.

Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', '2016-08-25 14:44:21', 2, 30);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (84, 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', '2017-01-15 09:59:22', 6, 16);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (85, 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', '2016-04-24 03:30:27', 4, 8);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (86, 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2016-06-18 13:08:44', 9, 12);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (87, 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', '2017-01-19 22:42:27', 1, 15);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (88, 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.

Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2016-07-22 21:19:29', 1, 2);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (89, 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', '2016-03-29 13:54:21', 6, 12);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (90, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus.', '2016-09-13 04:57:03', 2, 22);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (91, 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', '2016-04-18 08:14:28', 7, 33);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (92, 'Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.

Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', '2016-10-04 23:44:57', 6, 20);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (93, 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.

Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', '2016-04-13 15:40:21', 6, 11);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (94, 'Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.

In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', '2016-10-08 07:27:32', 10, 19);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (95, 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', '2016-11-30 02:27:21', 7, 8);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (96, 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.', '2017-02-17 07:57:20', 4, 18);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (97, 'Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', '2017-03-17 01:55:56', 5, 14);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (98, 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.

Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', '2016-05-02 13:51:42', 8, 31);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (99, 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', '2017-02-25 03:45:23', 4, 4);
insert into Comment (id, content, commentTimestamp, fashionee_id, wearable_id) values (100, 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', '2016-10-31 11:57:23', 10, 8);
