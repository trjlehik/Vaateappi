Educloud-Virtuaalipalvelin: 10.114.32.149
Http-portti: 80
Tietokantaportti: 3306

Vaateäpp
====

Tavoitteet
---

Vaateäpillä yhdistellään kuvia vaatteista ja asusteista asukokonaisuuksiksi,
jotka tallennetaan kuvakollaasina. Tallentamisen jälkeen asukokonaisuudet voi
julkaista yhteisön kommentointia varten, ja/tai tallentaa omaan käyttöön
muistilistaksi vaatekaapin sisällöstä. Asukuvia selailemalla saa ideoita
pukeutumiseen ja vaatteiden uudenlaiseen yhdistelyyn.

Asiakkaat ja käyttäjät
---

Ensisijainen käyttäjäryhmä on muodista ja vaatteista kiinnostuneet, kuten
muotibloggarit, jotka haluavat esitellä asujaan ja kokeilla etsiä uusia tapoja
yhdistellä olemassaolevia vaatekappaleitaan. Stylistit, jotka suunnittelevat
asukokonaisuuksia asiakkailleen ja esittelevät palveluitaan. Muotisuunnittelijat
ja tilausompelijat, jotka haluavat esitellä luomuksiaan, sekä saada näkyvyyttä
ja uusia asiakkaita. Netissä toimivien vaatekauppojen pitäjät ja asiakkaat,
jotka esittelevä ja kokeilevat asuja. Sekä tietenkin myös muut vaatteista ja
asukokonaisuuksista kiinnostuneet

Tarpeet
---

Käyttäjät haluavat sähköisen muistilistan siitä, mitä vaatteita omistavat ja
mitä on toivomuslistalla. He haluavat myös yhdistellä vaatteita
asukokonaisuuksiksi, tallentaa niitä ja saada palautetta muilta käyttäjiltä.
He haluavat etsiä uusia ideoita pukeutumiseen esim. muiden luomia kokonaisuuksia
selailemalla. Myös omista vaatteista saa jännittäviä uusia yhdistelmiä.

Kilpailutilanne
---

Vastaavia tuotteita ovat tällä hetkellä ainakin stylebookapp ja polyvore.
Linkit vastaaviin tuotteisiin:
http://www.stylebookapp.com/ ja http://www.polyvore.com/collage_maker/

Tuotteemme eroaa vastaavista kilpailijoista ainakin sen vuoksi,
että se ei ole alustasidonnainen ja sinne voi lisätä omia kuvia
asuista, taustoista, rekvisiitasta yms

====PHPUNIT & CLOUD9======

Komentorivikehotteet PHPUnitin ja tarvittavien kirjastojen lataamiseen.

wget https://phar.phpunit.de/phpunit-4.8.35.phar
chmod +x phpunit-4.8.35.phar
sudo mv phpunit-4.8.35.phar /usr/local/bin/phpunit
cd CodeIgniter-3.1.3
composer require phpunit/phpunit 4.8.35 --dev
composer require kenjis/ci-phpunit-test --dev
php vendor/kenjis/ci-phpunit-test/install.php

Testit ajataan CodeIgniter-3.1.3/application/tests -kansiossa
komennolla: phpunit

Yksittäisen testin voi ajaa esim. komennolla:
phpunit models/Category_model_test.php

Testit sijoitetaan CodeIgniter-3.1.3/application/tests -kansioon.

Lue lisää testien tekemisestä ym.
https://github.com/kenjis/ci-phpunit-test