
SET FOREIGN_KEY_CHECKS = 0;
DELETE FROM `c9`.`Category` WHERE `Category`.`id` = 1;
DELETE FROM `c9`.`Category` WHERE `Category`.`id` = 2;
DELETE FROM `c9`.`Category` WHERE `Category`.`id` = 3;
DELETE FROM `c9`.`Category` WHERE `Category`.`id` = 4;
DELETE FROM `c9`.`Category` WHERE `Category`.`id` = 5;
DELETE FROM `c9`.`Category` WHERE `Category`.`id` = 6;
DELETE FROM `c9`.`Category` WHERE `Category`.`id` = 7;
SET FOREIGN_KEY_CHECKS = 1;

INSERT INTO `Category` VALUES (1,'Legwear'),(2,'Jacket'),(3,'Shirt'),(4,'Knit'),(5,'Dress'),(6,'Shoes'),(7,'Accessories');